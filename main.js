function updateTime() {
  const now = new Date();
  const cities = {
    "new-york": now,
    london: new Date(now.getTime() + (now.getTimezoneOffset() + 60) * 60000),
    sydney: new Date(now.getTime() + (now.getTimezoneOffset() + 660) * 60000),
    tokyo: new Date(now.getTime() + (now.getTimezoneOffset() + 540) * 60000),
    "cape-town": new Date(
      now.getTime() + (now.getTimezoneOffset() + 120) * 60000,
    ),
    "rio-de-janeiro": new Date(
      now.getTime() + (now.getTimezoneOffset() - 180) * 60000,
    ),
    moscow: new Date(now.getTime() + (now.getTimezoneOffset() + 180) * 60000),
    "los-angeles": new Date(
      now.getTime() + (now.getTimezoneOffset() - 480) * 60000,
    ),
  };

  for (const city in cities) {
    const time = cities[city].toLocaleTimeString();
    document.querySelector(`#${city} .time`).textContent = time;
  }
}

setInterval(updateTime, 1000);
